var ClassicEditor;
var locale;

var confirmButtons = "" +
    "<button id='modal-ok' class='btn btn-success'></button> " +
    "<button id='modal-cancel' class='btn btn-danger'></button> ";

var modalCtl = {
    open: function (title, content, sizeOpt) {
        var size = "";
        if (typeof sizeOpt !== "undefined") {
            size = "modal-" + sizeOpt;
        }
        $("#modal .modal-dialog").removeClass("modal-lg");
        $("#modal .modal-dialog").removeClass("modal-sm");
        
        console.log(size);
        
        if (size !== "") {
            $("#modal .modal-dialog").addClass(size);
        }
        
        $("#modal .modal-title").html(title);
        $("#modal .modal-body").html(content);
        $("#modal").modal();
    },
    load: function (title, url) {
        $.get(url, {}, function (response) {
            $("#modal .modal-title").html(title);
            $("#modal .modal-body").html(response);
            $("#modal").modal();
        });
    },
    close: function () {
        $("#modal").modal("hide");
    },
    confirmDelete: function (message, url, cancelText, okText, title) {
        this.open(title, "<p>" + message + "</p>" + confirmButtons);
        $("#modal").one("shown.bs.modal", function () {
            $("#modal-cancel").text(cancelText);
            $("#modal-ok").text(okText);
            $("#modal-cancel").one("click", function () {
                $("#modal").modal("hide");
            });
            $("#modal-ok").one("click", function () {
                window.location.href = url;
            });
        });
    }
};


$(function () {
    if ($(document.querySelector('textarea.richtext')).length > 0) {
        var editor = ClassicEditor
            .create(document.querySelector('textarea.richtext'), {
                language: locale
        });
    }
});
