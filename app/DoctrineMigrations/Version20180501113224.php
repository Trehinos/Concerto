<?php declare(strict_types = 1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180501113224 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        
        $this->addSql('CREATE TABLE parameter (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, args LONGTEXT DEFAULT NULL, value LONGTEXT DEFAULT NULL, UNIQUE INDEX UNIQ_2A9791105E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql("INSERT INTO `parameter` (`id`, `name`, `type`, `args`, `value`) VALUES
(1, 'Text parameter', 'TEXT', NULL, 'Parameter Text'),
(2, 'RichText parameter', 'RICHTEXT', NULL, '<p>Parameter <strong>Text</strong></p>'),
(3, 'Number parameter', 'NUMBER', '0;100;2', '12.5'),
(4, 'Integer parameter', 'INTEGER', '-50;50', '0'),
(5, 'Choice parameter', 'CHOICE', 'Choice 1;Choice 2;Another choice', '2'),
(6, 'Entity parameter', 'ENTITY', 'ConcertoBundle:User', NULL);");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE parameter');
    }
}
