# Concerto
A working start project for Symfony 3.4.
Instead of rewritting the same code for every new project, use **Concerto** to
start coding immediatly.  
All features are implemented with Symfony good practices in order to let you add and edit
your code the way you want. **Concerto** is not meant to change the way you already develop
your Symfony applications but to let you do it faster.

## Used Libraries
* [Symfony 3.4](https://symfony.com/doc/3.4/setup.html) : Base framework
* [Symfony Assetic](https://symfony.com/doc/3.4/frontend/assetic/asset_management.html) : Asset management
* [Doctrine Migrations](https://symfony.com/doc/master/bundles/DoctrineMigrationsBundle/index.html) : Generate and migrate database
* [Ramsey Uuid Doctrine](https://github.com/ramsey/uuid-doctrine) : Generate UUID
* [JQuery 3.3.1](https://jquery.com/) : Javascript coding improve
* [Bootstrap 4.1](https://getbootstrap.com/) : CSS Framework
* [Fontawesome 5.0.10](https://fontawesome.com/) : Icons and icons utilities
* [CKEditor 5](https://ckeditor.com/ckeditor-5-builds/) : RichText editor

## Requirements
* HTTP server (tested with Apache2)
* PHP 7.2+ (could work on PHP 7.0 but not tested)
* MySQL (for migrations : you can use an other DBMS but Doctrine Migrations will be unavailable.)
* Composer

## Demo application
[See demo](https://concerto.trehinos.eu) : Use **admin**/**admin** as username/password. In the demo, you cannot add a user or
edit the admin account.

## Features
* Bootstrap 4 and FontAwesome 5.
* Improved BaseController class and BaseManager.
* Default use of Bootstrap 4 in forms.
* User management and roles.
* Security and login.
* Database parameters.
* Internationalization (supported locales : ```en``` and ```fr``` (feel free to create your own)).
* Menu system (see ```app/config/menu.yml```).
* Theming & theme **Crescendo**.

## Install
1. Git clone the project :

    ```
    git clone -b master http://gitlab.trehinos.eu/root/concerto.git
    ```
    
2. Run composer install :

    ```
    composer install
    ```
    
3. Update ```app/config/parameters.yml```
4. Setup database :

    ```
    php bin/console doctrine:database:create
    php bin/console doctrine:migrations:migrate --no-interaction
    ```
    
5. Dump assets :

    ```
    php bin/console assetic:dump --env=prod
    ```

6. Make folder ```web/files``` writable for the server.
    
## Get started
### Shortcuts
There are many shortcuts you can use in your controllers :

```php
// Instead of typing
$this->getDoctrine()->getManager()->persist($entity);
$this->getDoctrine()->getManager()->remove($entity);
$this->getDoctrine()->getManager()->flush();

$this->getDoctrine()->getRepository($entityName)->find($id);
$this->getDoctrine()->getRepository($entityName)->findAll();

$session->getFlashBag()->add($type, $message);

$this->render('view.html.twig', $params);

// Type
$this->manager()->persist($entity);
$this->manager()->remove($entity);
$this->manager()->flush();

$this->manager()->find($entityName, $id);
$this->manager()->findAll($entityName);

$this->addMessage($type, $message);

$this->viewHtml('view', $params);

// Access the translator
$this->translator();

// Get the menu (array)
$this->getMenu();

// Manager the parameters
$this->getDbParameter($name);
$this->setDbParameter($name, $value);
// Find all parameters
public function controller(ParameterManager $paramManager)
{
    $parameters = $paramManager->findAll();
}

```

### Menu configuration
The menu is configured in ```app/config/menu.yml```. This is a complete sample :

```yaml
menu:
    # The main icon of the application (Fontawesome class without fa- prefix)
    brand-icon: icon
    # The route name when clicking on the icon
    brand-route: routename

    # Main items
    items:
        # Define items with route OR dropdown. You're free to choose your item names.
        item_name_1:
            icon: icon
            route: routename
            # The text can be raw text or a key in translation files
            text: Text below icon

        item_name_2:
            icon: icon
            text: Text below icon
            # You can limit the display of an item by defining a required ROLE
            role: ROLE_
            # It's a dropdown item
            dropdown:
                # Define subitems (same options as classic items)
                sub_item:
                    icon: icon
                    text: Text below icon
                    route: routename
                    
    # Optionnal : display user button (if user_options is defined, all suboptions are required)
    user_options:
        display_username: true
        logout-route: logoutroute
        profile-route: update-profile
        password-route: update-password
        
```

### Create a new page
1. Create your (or use on of yours) controller(s). The controller shall extend ```Trehinos\ConcertoBundle\Controller\BaseController```.
2. Make your template extend the theme template with ```{% extends theme %}```. The variable ```theme``` is set in ```app/config/parameters.yml``` (default: ```concerto_theme: Themes/theme-crescendo.html.twig```).
3. Add what you want : it's exactly like a base Symfony with some magic =)

### Users
After setup, the only user created is admin with password admin. **Set the password at your
first login**.

In concerto, there are three default roles :
* ROLE_ADMIN : Admninistrator, can manage parameters.
* ROLE_USER_MANAGE : Can manage users.
* ROLE_USER : Can sign in and use application.

### Parameters in database
#### General parameters
You can add parameters directly in database in order to let your users (with ROLE_ADMIN)
edit it. Use ```$this->getDbParameter()``` in a controller to get the value of a parameter.  
There are many types of parameter. Each type can have options :

* **TEXT** (max length) : displays a text field.
* **RICHTEXT** : displays a CKEditor.
* **NUMBER** (min, max, decimals) : displays a number field.
* **INTEGER** (min, max) : displays a number field.
* **CHOICE** (choices) : displays a combo-box field with choices.
* **ENTITY** (Bundle:Entity) : displays a combo-box field with all entities. The entity MUST override ```__toString()``` method.
* **FILE** (MimeTypes, max size (in KB)) : displays a file download button and a file upload field.
* **IMAGE** (MimeTypes, max size (in KB)) : displays an image preview and a file upload field.

For now, you have to add/edit/remove parameters directly in your database.

Don't forget to create the ```web/files``` folder and give the server the right to write in. And don't forget
to set your ```upload_max_filesize``` in your ```php.ini```.

#### Users parameters
Parameters can be set for users when the field ```userSetting``` is set to 1 (instead of NULL or 0).

### Set a new locale
* Copy files ```app/Resources/translations/messages.en.yml``` and ```app/Resources/translations/messages.en.yml``` and set
all strings in your language.
* Set the locale in ```app/config/parameters.yml```.
* In ```app/Resources/views/Concerto/bootstrap-base.html.twig```, change the line ```'libs/ckeditor/translations/fr.js'``` with your
locale (this is the locale of CKEditor. Only locales packaged with CKEditor are available here).

### Themes
The theme is defined in ```app/config/parameters.yml``` :

```yaml
concerto_theme: Themes/your-theme.html.twig
```

You can create a personal stylesheet in ```web/themes/your-theme.css```.  
Look at the default theme **crescendo** to learn more about theming.

## License
[MIT](https://opensource.org/licenses/MIT) (c) Trehinos <trehinos@gmail.com>