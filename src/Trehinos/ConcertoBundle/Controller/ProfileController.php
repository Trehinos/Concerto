<?php

/**
 * @author Trehinos <trehinos@gmail.com>
 * @since v0.4
 * @license MIT License
 *
 * Copyright (c) 2018 Trehinos <trehinos@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace Trehinos\ConcertoBundle\Controller;

use Trehinos\ConcertoBundle\Controller\BaseController;
use Symfony\Component\Routing\Annotation\Route;
use Trehinos\ConcertoBundle\Manager\UserManager;
use Trehinos\ConcertoBundle\Form\UserType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Trehinos\ConcertoBundle\Manager\MenuManager;
use Trehinos\ConcertoBundle\Manager\BaseManager;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * ProfileController display and handle the forms permitting a user to edit his profile or his password.
 * 
 * @Route("/profile")
 */
class ProfileController extends BaseController
{

    const ACTION_PASSWORD = "password";
    const ACTION_PROFILE  = "profile";

    private $userManager;

    /**
     * 
     * @param UserManager $manager
     */
    public function __construct(
    MenuManager $menuManager, BaseManager $baseManager, TranslatorInterface $translator, UserManager $manager
    )
    {
        parent::__construct($menuManager, $baseManager, $translator);
        $this->userManager = $manager;
    }

    /**
     * @Route("/password", name="update-password")
     */
    public function password(Request $request)
    {
        return $this->form($request, self::ACTION_PASSWORD);
    }

    /**
     * @Route("/profile", name="update-profile")
     */
    public function profile(Request $request)
    {
        return $this->form($request, self::ACTION_PROFILE);
    }

    /**
     * 
     * @param Request $request
     * @param string $action
     * 
     * @return Response
     */
    public function form(Request $request, string $action)
    {
        if (self::ACTION_PASSWORD !== $action && self::ACTION_PROFILE !== $action) {
            throw new NotFoundHttpException();
        }
        $user = $this->getUser();

        $form = $this->createForm(UserType::class, $user);
        $form->remove('roles');

        if (self::ACTION_PASSWORD === $action) {
            $form->remove('username')->remove('email');
        } else if (self::ACTION_PROFILE === $action) {
            $form->remove('password');
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();

            if (self::ACTION_PASSWORD === $action) {
                $user = $this->userManager->setPassword($user, $user->getPassword());
                $this->addMessage('success', 'password_updated');
            } else {
                $this->addMessage('success', 'profile_updated');
            }

            $this->manager()->persist($user);
            $this->manager()->flush();

            $session = new Session();
            $lastUrl = $session->get('saved_url');

            if (!$lastUrl || strrpos($lastUrl, '/login') !== false) {
                return $this->redirectToRoute('index');
            }

            return $this->redirect($lastUrl);
        } else if (!$form->isSubmitted()) {
            $lastUrl = $request->headers->get('referer');
            $session = new Session();
            $session->set('saved_url', $lastUrl);
        }

        return $this->viewHtml('Concerto/User/profile', [
                'form'   => $form->createView(),
                'action' => $action
        ]);
    }

}
