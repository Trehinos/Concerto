<?php

/**
 * @author Trehinos <trehinos@gmail.com>
 * @since v0.5
 * @license MIT License
 *
 * Copyright (c) 2018 Trehinos <trehinos@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace Trehinos\ConcertoBundle\Controller;

use Trehinos\ConcertoBundle\Controller\BaseController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Trehinos\ConcertoBundle\Manager\ParameterManager;
use Trehinos\ConcertoBundle\Entity\Parameter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/user/parameters")
 * 
 */
class UserParameterController extends BaseController
{

    /**
     * 
     * @param ParameterManager $paramManager
     * 
     * @return Response
     * 
     * @Route("/list", name="list-user-parameters")
     */
    public function listParameters(ParameterManager $paramManager)
    {
        $parameters        = $paramManager->findAllUserParameters();
        $entityCollections = [];

        foreach ($parameters as $parameter) {
            if ($parameter->getType() == Parameter::TYPE_ENTITY) {
                $entityCollections[$parameter->getArgs()] = $this->manager()->findAll($parameter->getArgs());
            }
        }

        return $this->viewHtml('Concerto/Parameter/list', [
                'parameters'        => $parameters,
                'user'              => $this->getUser(),
                'entityCollections' => $entityCollections
        ]);
    }

    /**
     * 
     * @param Request $request
     * @param ParameterManager $paramManager
     * 
     * @return Response
     * 
     * @Route("/save", name="save-user-parameters")
     */
    public function saveParameters(Request $request, ParameterManager $paramManager)
    {
        $parameters = $paramManager->findAllUserParameters();
        $user       = $this->getUser();

        foreach ($parameters as $parameter) {
            $value = $request->request->get($parameter->getHtmlId());
            if (in_array($parameter->getType(), [Parameter::TYPE_FILE, Parameter::TYPE_IMAGE])) {
                if ($request->request->get($parameter->getHtmlId() . '-delete') == 'on') {
                    $currentFileName = $user->getUserParameter($parameter->getId());
                    if ($currentFileName !== null && $currentFileName !== '') {
                        unlink("files/$currentFileName");
                    }
                    $paramManager->setUserParameter($user, $parameter->getId(), null);
                } else {
                    $value = $paramManager->uploadFile($request, $parameter);
                    if (is_array($value)) {
                        $this->addMessage($value[0], $this->translator()->trans($value[1]).(isset($value[2]) ? ' ('.$value[2].')' : ''));
                        return $this->redirectToRoute('list-parameters');
                    } elseif ($value !== '') {
                        $paramManager->setUserParameter($user, $parameter->getId(), $value);
                    }
                }
            } else {
                $paramManager->setUserParameter($user, $parameter->getId(), $value);
            }
        }

        $this->addMessage('success', 'parameters_updated');

        return $this->redirectToRoute('list-user-parameters');
    }

}
