<?php

/**
 * @author Trehinos <trehinos@gmail.com>
 * @since v0.4
 * @license MIT License
 *
 * Copyright (c) 2018 Trehinos <trehinos@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace Trehinos\ConcertoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Trehinos\ConcertoBundle\Manager\MenuManager;
use Trehinos\ConcertoBundle\Manager\BaseManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Trehinos\ConcertoBundle\Manager\ParameterManager;

/**
 * BaseController is the base controller used in all Concerto controller classes.
 * Feel free to use it and its usefull shortcuts.
 */
class BaseController extends Controller
{

    /**
     * @var MenuManager 
     */
    private $menuManager;

    /**
     * @var BaseManager
     */
    private $baseManager;

    /**
     * @var Translator
     */
    private $translator;

    /**
     * 
     */
    private $parametersManager;

    /**
     * @param MenuManager $menuManager
     * @param BaseManager $baseManager
     * @param TranslatorInterface $translator
     */
    public function __construct(
    MenuManager $menuManager, BaseManager $baseManager, TranslatorInterface $translator
    )
    {
        $this->menuManager       = $menuManager;
        $this->baseManager       = $baseManager;
        $this->translator        = $translator;
        $this->parametersManager = new ParameterManager($baseManager);
    }

    /**
     * @return array
     */
    public function getMenu()
    {
        return $this->menuManager->getMenu();
    }

    /**
     * @return BaseManager
     */
    public function manager()
    {
        return $this->baseManager;
    }

    /**
     * @return Translator
     */
    public function translator()
    {
        return $this->translator;
    }

    /**
     * @param string $view
     * @param array $vars
     * 
     * @return Response
     */
    public function viewHtml(string $view, array $vars)
    {
        return $this->render("$view.html.twig", $vars);
    }

    /**
     * Add message in the FlashBag.
     * 
     * @param type $type
     * @param type $message
     */
    public function addMessage($type, $message)
    {
        $session = new Session();
        $session->getFlashBag()->add($type, $message);
    }

    public function getDbParameter(string $name)
    {
        $this->parametersManager->get($name);
    }

    public function setDbParameter(string $name, string $value)
    {
        $this->parametersManager->set($name, $value);
    }

}
