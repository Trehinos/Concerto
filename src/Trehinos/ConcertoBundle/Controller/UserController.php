<?php

/**
 * @author Trehinos <trehinos@gmail.com>
 * @since v0.4
 * @license MIT License
 *
 * Copyright (c) 2018 Trehinos <trehinos@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace Trehinos\ConcertoBundle\Controller;

use Trehinos\ConcertoBundle\Controller\BaseController;
use Trehinos\ConcertoBundle\Manager\UserManager;
use Trehinos\ConcertoBundle\Entity\User;
use Trehinos\ConcertoBundle\Form\UserType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Trehinos\ConcertoBundle\Manager\MenuManager;
use Trehinos\ConcertoBundle\Manager\BaseManager;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/users")
 * 
 * @Security("has_role('ROLE_USER_MANAGE')")
 */
class UserController extends BaseController
{
    
    const ACTION_ADD = 'add';
    const ACTION_EDIT = 'edit';

    private $userManager;

    /**
     * @param MenuManager $menuManager
     * @param BaseManager $baseManager
     * @param TranslatorInterface $translator
     * @param UserManager $manager
     */
    public function __construct(
        MenuManager $menuManager,
        BaseManager $baseManager,
        TranslatorInterface $translator,
        UserManager $manager
    )
    {
        parent::__construct($menuManager, $baseManager, $translator);
        $this->userManager = $manager;
    }

    /**
     * @Route("/list", name="list-users")
     * 
     * @return Response
     */
    public function listUsers()
    {
        $users = $this->userManager->findAll();

        return $this->viewHtml('Concerto/User/list-users', [
                'users' => $users
        ]);
    }

    /**
     * @Route("/add", name="add-user")
     * 
     * @param Request $request
     * 
     * @return Response
     */
    public function addUser(Request $request)
    {
        $user = new User();

        return $this->formUser($request, $user);
    }

    /**
     * @Route("/edit/{id}", name="edit-user")
     * 
     * @param Request $request
     * @param User $user
     * 
     * @return Response
     */
    public function editUser(Request $request, User $user)
    {
        return $this->formUser($request, $user, self::ACTION_EDIT);
    }

    /**
     * @param Request $request
     * @param User $user
     * @param string $action
     * 
     * @return type
     */
    public function formUser(Request $request, User $user, string $action = self::ACTION_ADD)
    {
        $form = $this->createForm(UserType::class, $user);
        
        if (self::ACTION_EDIT === $action) {
            $form->remove('password');
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            
            $user->setRoles($form->get('roles')->getData());
            
            if (self::ACTION_ADD === $action) {
                $user = $this->userManager->setPassword($user, $user->getPassword());
                $this->addMessage('success', 'user_added');
            }
            else {
                $this->addMessage('success', 'user_edited');
            }
            
            $this->manager()->persist($user);
            $this->manager()->flush();
            
            return $this->redirectToRoute('list-users');
        }
        
        return $this->viewHtml('Concerto/User/form', [
            'form' => $form->createView(),
            'action' => $action
        ]);
    }
    
    /**
     * @Route("/remove/{id}", name="remove-user")
     *
     * @param User $user
     * @return Response
     */
    public function removeUser(User $user)
    {
        $this->manager()->remove($user);
        $this->manager()->flush();
        
        $this->addMessage('success', 'user_removed');
        
        return $this->redirectToRoute('list-users');
    }

}
