<?php

/**
 * @author Trehinos <trehinos@gmail.com>
 * @since v0.5
 * @license MIT License
 *
 * Copyright (c) 2018 Trehinos <trehinos@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace Trehinos\ConcertoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserParameter
 *
 * @ORM\Table(name="user_parameter")
 * @ORM\Entity(repositoryClass="Trehinos\ConcertoBundle\Repository\UserParameterRepository")
 */
class UserParameter
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255)
     */
    private $value;

    /**
     * @var User
     * 
     * @ORM\ManyToOne(targetEntity="User", inversedBy="userParameters")
     */
    private $user;

    /**
     * @var Parameter
     * 
     * @ORM\ManyToOne(targetEntity="Parameter", inversedBy="userParameters")
     */
    private $parameter;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value.
     *
     * @param string $value
     *
     * @return UserParameter
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value.
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set user.
     *
     * @param \Trehinos\ConcertoBundle\Entity\User|null $user
     *
     * @return UserParameter
     */
    public function setUser(\Trehinos\ConcertoBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \Trehinos\ConcertoBundle\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set parameter.
     *
     * @param \Trehinos\ConcertoBundle\Entity\Parameter|null $parameter
     *
     * @return UserParameter
     */
    public function setParameter(\Trehinos\ConcertoBundle\Entity\Parameter $parameter = null)
    {
        $this->parameter = $parameter;

        return $this;
    }

    /**
     * Get parameter.
     *
     * @return \Trehinos\ConcertoBundle\Entity\Parameter|null
     */
    public function getParameter()
    {
        return $this->parameter;
    }

}
