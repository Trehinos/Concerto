<?php

/**
 * @author Trehinos <trehinos@gmail.com>
 * @since v0.5
 * @license MIT License
 *
 * Copyright (c) 2018 Trehinos <trehinos@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace Trehinos\ConcertoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Parameter
 *
 * @ORM\Table(name="parameter")
 * @ORM\Entity(repositoryClass="Trehinos\ConcertoBundle\Repository\ParameterRepository")
 * @UniqueEntity(fields="name", message="dup_parameter")
 */
class Parameter
{

    const TYPE_TEXT     = 'TEXT';
    const TYPE_RICHTEXT = 'RICHTEXT';
    const TYPE_NUMBER   = 'NUMBER';
    const TYPE_INTEGER  = 'INTEGER';
    const TYPE_CHOICE   = 'CHOICE';
    const TYPE_FILE     = 'FILE';
    const TYPE_IMAGE    = 'IMAGE';
    const TYPE_ENTITY   = 'ENTITY';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     * 
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     * 
     * @Assert\NotBlank()
     */
    private $type;

    /**
     * @var string|null
     *
     * @ORM\Column(name="args", type="text", nullable=true)
     */
    private $args;

    /**
     * @var string|null
     *
     * @ORM\Column(name="value", type="text", nullable=true)
     */
    private $value;

    /**
     * @var bool|null
     * 
     * @ORM\Column(name="userSetting", type="boolean", nullable=true)
     */
    private $userSetting;

    /**
     * @var array
     * 
     * @ORM\OneToMany(targetEntity="UserParameter", mappedBy="parameter")
     */
    private $userParameters;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Parameter
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return Parameter
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set args.
     *
     * @param string|null $args
     *
     * @return Parameter
     */
    public function setArgs($args = null)
    {
        $this->args = $args;

        return $this;
    }

    /**
     * Get args.
     *
     * @return string|null
     */
    public function getArgs()
    {
        return $this->args;
    }

    /**
     * Set value.
     *
     * @param string|null $value
     *
     * @return Parameter
     */
    public function setValue($value = null)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value.
     *
     * @return string|null
     */
    public function getValue()
    {
        return $this->value;
    }

    public function getHtmlId()
    {
        return $output = strtolower(str_replace([
            ' '
                ], [
            '-'
                ], $this->name));
    }

    /**
     * Set userSetting.
     *
     * @param bool|null $userSetting
     *
     * @return Parameter
     */
    public function setUserSetting($userSetting = null)
    {
        $this->userSetting = $userSetting ? true : null;

        return $this;
    }

    /**
     * Get userSetting.
     *
     * @return bool|null
     */
    public function getUserSetting()
    {
        return $this->userSetting ?? false;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->userParameter = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add userParameter.
     *
     * @param \Trehinos\ConcertoBundle\Entity\UserParameter $userParameter
     *
     * @return Parameter
     */
    public function addUserParameter(\Trehinos\ConcertoBundle\Entity\UserParameter $userParameter)
    {
        $this->userParameters[] = $userParameter;

        return $this;
    }

    /**
     * Remove userParameter.
     *
     * @param \Trehinos\ConcertoBundle\Entity\UserParameter $userParameter
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeUserParameter(\Trehinos\ConcertoBundle\Entity\UserParameter $userParameter)
    {
        return $this->userParameters->removeElement($userParameter);
    }

    /**
     * Get userParameters.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserParameters()
    {
        return $this->userParameters;
    }

}
