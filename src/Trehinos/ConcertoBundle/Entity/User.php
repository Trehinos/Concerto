<?php

/**
 * @author Trehinos <trehinos@gmail.com>
 * @since v0.4
 * @license MIT License
 *
 * Copyright (c) 2018 Trehinos <trehinos@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace Trehinos\ConcertoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * User entity for authentification.
 *
 * @ORM\Table(name="app_user")
 * @ORM\Entity(repositoryClass="Trehinos\ConcertoBundle\Repository\UserRepository")
 * @UniqueEntity(fields="username", message="dup_username")
 * @UniqueEntity(fields="email", message="dup_email")
 */
class User implements UserInterface, \Serializable
{

    const ROLE_ADMIN       = 'ROLE_ADMIN';
    const ROLE_USER_MANAGE = 'ROLE_USER_MANAGE';
    const ROLE_USER        = 'ROLE_USER';
    const ROLES = [
        self::ROLE_ADMIN       => self::ROLE_ADMIN,
        self::ROLE_USER_MANAGE => self::ROLE_USER_MANAGE,
        self::ROLE_USER        => self::ROLE_USER
    ];

    /**
     * @var UuidInterface
     *
     * @ORM\Column(name="id", type="uuid", unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255, unique=true)
     * 
     * @Assert\NotBlank()
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     * 
     * @Assert\Email()
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     * 
     * @Assert\Length(min=8)
     */
    private $password;

    /**
     * @var array
     * 
     * @ORM\Column(name="roles", type="json_array")
     * 
     */
    private $roles;

    /**
     * @var array
     * 
     * @ORM\OneToMany(targetEntity="UserParameter", mappedBy="user")
     */
    private $userParameters;

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id->toString();
    }

    /**
     * Set username.
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername(string $username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail(string $email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password.
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword(string $password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Do nothing.
     * 
     * @return null
     */
    public function eraseCredentials()
    {
        return null;
    }

    /**
     * Get the user's roles.
     * 
     * @return array
     */
    public function getRoles()
    {
        $roles   = $this->roles;
        $roles[] = self::ROLE_USER;

        return array_unique($roles);
    }

    /**
     * Do nothing.
     * 
     * @return null
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Add one role to the user.
     * 
     * @param string $role
     * @return User
     */
    public function addRole(string $role)
    {
        $this->roles[] = $role;

        return $this;
    }

    /**
     * Set the user's roles.
     * 
     * @param array $roles
     * @return User
     */
    public function setRoles(array $roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /*
     * @see \Serializable
     */

    public function serialize()
    {
        return serialize([
            $this->id,
            $this->username,
            $this->password,
            $this->roles
        ]);
    }

    /**
     * 
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            $this->roles
            ) = unserialize($serialized);
    }

    public function __toString()
    {
        return $this->getUsername();
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->userParameter = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add userParameter.
     *
     * @param \Trehinos\ConcertoBundle\Entity\UserParameter $userParameter
     *
     * @return User
     */
    public function addUserParameter(\Trehinos\ConcertoBundle\Entity\UserParameter $userParameter)
    {
        $this->userParameters[] = $userParameter;

        return $this;
    }

    /**
     * Remove userParameter.
     *
     * @param \Trehinos\ConcertoBundle\Entity\UserParameter $userParameter
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeUserParameter(\Trehinos\ConcertoBundle\Entity\UserParameter $userParameter)
    {
        return $this->userParameters->removeElement($userParameter);
    }

    /**
     * Get userParameters.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserParameters()
    {
        return $this->userParameters;
    }

    public function getParameters()
    {
        $params = [];
        foreach ($this->getUserParameters() as $up) {
            $params[] = $up->getParameter();
        }

        return $params;
    }
    
    public function getParameter($id)
    {
        foreach ($this->getUserParameters() as $up) {
            if ($up->getParameter()->getId() === $id) {
                return $up->getParameter();
            }
        }

        return null;
    }
    
    public function getUserParameter($id)
    {
        foreach ($this->getUserParameters() as $up) {
            if ($up->getParameter()->getId() === $id) {
                return $up->getValue();
            }
        }

        return null;
    }

}
