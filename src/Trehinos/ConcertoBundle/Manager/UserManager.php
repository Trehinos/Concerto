<?php

/**
 * @author Trehinos <trehinos@gmail.com>
 * @since v0.4
 * @license MIT License
 *
 * Copyright (c) 2018 Trehinos <trehinos@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace Trehinos\ConcertoBundle\Manager;

use Trehinos\ConcertoBundle\Entity\User;
use Trehinos\ConcertoBundle\Manager\BaseManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * UserManager service.
 */
class UserManager
{

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;
    
    /**
     * @var BaseManager
     */
    private $manager;

    /**
     * @param BaseManager $manager
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(
        BaseManager $manager, UserPasswordEncoderInterface $encoder
    )
    {
        $this->manager = $manager;
        $this->encoder = $encoder;
    }

    /**
     * Returns one entity of given type and id.
     * 
     * @param mixed $id
     * 
     * @return mixed
     */
    public function find($id)
    {
        return $this->manager->find('ConcertoBundle:User', $id);
    }

    /**
     * Returns all entities of a given type.
     * 
     * @return array
     */
    public function findAll()
    {
        return $this->manager->findAll('ConcertoBundle:User');
    }

    public function exists($username)
    {
        $user = $this->manager->getEntityManager()->getRepository('ConcertoBundle:User')->findByUsername($username);
        return null !== $user;
    }

    /**
     * @param User $user
     * @param string $pwd
     * 
     * @return User
     */
    public function setPassword(User $user, string $pwd)
    {
        $encoded_pwd = $this->encoder->encodePassword($user, $pwd);
        $user->setPassword($encoded_pwd);
        
        return $user;
    }

}
