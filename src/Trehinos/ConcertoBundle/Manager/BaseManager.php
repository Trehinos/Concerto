<?php

/**
 * @author Trehinos <trehinos@gmail.com>
 * @since v0.4
 * @license MIT License
 *
 * Copyright (c) 2018 Trehinos <trehinos@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace Trehinos\ConcertoBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;

/**
 * BaseManager service.
 */
class BaseManager
{

    /**
     * @var EntityManagerInterface 
     */
    private $entityManagerService;

    /**
     * @param EntityManagerInterface $entityManagerService
     */
    public function __construct(EntityManagerInterface $entityManagerService)
    {
        $this->entityManagerService = $entityManagerService;
    }

    /**
     * Returns the Doctrine ObjectManager.
     * 
     * @return \Doctrine\Common\Persistence\ObjectManager
     */
    public function getEntityManager()
    {
        return $this->entityManagerService;
    }

    /**
     * Returns one entity of given type and id.
     * 
     * @param string $entity
     * @param mixed $id
     * 
     * @return mixed
     */
    public function find(string $entity, $id)
    {
        return $this->entityManagerService->getRepository($entity)->find($id);
    }

    /**
     * Returns all entities of a given type.
     * 
     * @param string $entity
     * 
     * @return array
     */
    public function findAll(string $entity)
    {
        return $this->entityManagerService->getRepository($entity)->findAll();
    }

    /**
     * Tells the ObjectManager to make an instance managed and persistent.
     * 
     * @param mixed $entity
     */
    public function persist($entity)
    {
        $this->entityManagerService->persist($entity);
    }

    /**
     * Removes an object instance.
     * 
     * @param mixed $entity
     */
    public function remove($entity)
    {
        $this->entityManagerService->remove($entity);
    }

    /**
     * Flushes all changes to objects that have been queued up to now to the database.
     * This effectively synchronizes the in-memory state of managed objects with the
     * database.
     */
    public function flush()
    {
        $this->entityManagerService->flush();
    }

}
