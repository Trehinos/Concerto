<?php

/**
 * @author Trehinos <trehinos@gmail.com>
 * @since v0.4
 * @license MIT License
 *
 * Copyright (c) 2018 Trehinos <trehinos@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace Trehinos\ConcertoBundle\Manager;

use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Yaml\Yaml;

use Trehinos\ConcertoBundle\Exception\MenuException;

/**
 * MenuManager is the loader reading the "/app/config/menu.yml" file.
 */
class MenuManager
{

    /**
     * The default location of menu.yml.
     */
    const CONFIG_FOLDER = '/app/config';

    /**
     * Symfony FileLocator instance for finding the config menu file.
     * 
     * @var FileLocator
     */
    private $fileLocator;

    /**
     * Constructs the MenuManager by injecting a FileLocator.
     */
    public function __construct(KernelInterface $kernel)
    {
        $root_folder = $kernel->getProjectDir();
        $this->fileLocator = new FileLocator($root_folder.self::CONFIG_FOLDER);
    }

    /**
     * Get an array representing the menu as defined in the menu.yml file.
     * 
     * @return array
     */
    public function getMenu()
    {
        $file = $this->fileLocator->locate('menu.yml', null, true);
        $menu = Yaml::parseFile($file);
        
        $this->validate($menu);

        return $menu['menu'];
    }
    
    /**
     * Validate the menu array structure.
     * 
     * @param array $menu
     */
    public function validate(array $menu) {
        $this->validateItem($menu, 'brand-route');
        $this->validateItem($menu, 'brand-icon');
        
        $items = $menu['items'] ?? [];
        foreach ($items as $item) {
            if (array_key_exists('dropdown', $item)) {
                $dropdown_items = $item['dropdown'];
                foreach ($dropdown_items as $dropdown_item) {
                    $this->validateItem($dropdown_item, 'icon');
                    $this->validateItem($dropdown_item, 'route');
                    $this->validateItem($dropdown_item, 'text');
                }
            } else {
                $this->validateItem($item, 'route');
            }
            $this->validateItem($item, 'icon');
            $this->validateItem($item, 'text');
        }
        
        if (array_key_exists('user_options', $menu)) {
            $this->validateItem($menu['user_options'], 'profile-route');
            $this->validateItem($menu['user_options'], 'password-route');
            $this->validateItem($menu['user_options'], 'logout-route');
        }
    }
    
    /**
     * @param type $array
     * @param string $key
     * 
     * @throws MenuException
     */
    public function validateItem($array, string $key) {
        if (isset($array[$key])) {
            throw new MenuException("$key doesn't exists in array.");
        }
    }

}
