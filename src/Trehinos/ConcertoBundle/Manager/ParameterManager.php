<?php

/**
 * @author Trehinos <trehinos@gmail.com>
 * @since v0.5
 * @license MIT License
 *
 * Copyright (c) 2018 Trehinos <trehinos@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace Trehinos\ConcertoBundle\Manager;

use Trehinos\ConcertoBundle\Manager\BaseManager;
use Trehinos\ConcertoBundle\Entity\Parameter;
use Trehinos\ConcertoBundle\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * ParameterManager service.
 */
class ParameterManager
{

    /**
     * @var BaseManager
     */
    private $manager;

    /**
     * @param BaseManager $manager
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(
    BaseManager $manager
    )
    {
        $this->manager = $manager;
    }

    /**
     * Returns one entity of given type and id.
     * 
     * @param mixed $id
     * 
     * @return mixed
     */
    public function find($id)
    {
        return $this->manager->find('ConcertoBundle:Parameter', $id);
    }

    /**
     * Returns one entity of given type and name.
     * 
     * @param string $name
     * 
     * @return mixed
     */
    public function findByName(string $name)
    {
        return $this->manager->getEntityManager()->getRepository('ConcertoBundle:Parameter')->findOneByName($name);
    }

    /**
     * Returns all entities of a given type.
     * 
     * @return array
     */
    public function findAllGeneralParameters()
    {
        return $this->manager->getEntityManager()->getRepository('ConcertoBundle:Parameter')->findByUserSetting(null);
    }
    
    /**
     * Returns all entities of a given type which are user settings
     * 
     * @return array
     */
    public function findAllUserParameters()
    {
        return $this->manager->getEntityManager()->getRepository('ConcertoBundle:Parameter')->findByUserSetting(true);
    }

    /**
     * 
     * @param string $name
     * @param $value
     * 
     * @return $this
     */
    public function set(string $name, $value)
    {
        $param = $this->findByName($name);
        $param->setValue($value);
        $this->manager->persist($param);
        $this->manager->flush();

        return $this;
    }
    
    /**
     * 
     * @param string $id
     * @param $value
     * 
     * @return $this
     */
    public function setUserParameter(User $user, string $id, $value)
    {
        $userParameter = $this->manager->getEntityManager()->getRepository('ConcertoBundle:UserParameter')->findOneBy([
            'parameter' => $id,
            'user' => $user->getId()
        ]);

        if (null === $userParameter) {
            $userParameter = new \Trehinos\ConcertoBundle\Entity\UserParameter();
            $param = $this->find($id);
            $userParameter->setUser($user);
            $userParameter->setParameter($param);
        }

        $userParameter->setValue($value);
        $this->manager->persist($userParameter);
        $this->manager->flush();
        
        
        return $this;
    }

    /**
     * 
     * @param string $name
     * @param string $type
     * @param string $value
     * @param string $args
     * 
     * @return $this
     */
    public function add(string $name, string $type, string $value, string $args)
    {
        $param = new Parameter();
        $param->setName($name);
        $param->setType($type);
        $param->setArgs($args);
        $param->setValue($value);

        $this->manager->persist($param);
        $this->manager->flush();
        
        return $this;
    }

    /**
     * 
     * @param string $name
     * 
     * @return type
     */
    public function get(string $name)
    {
        $param = $this->findByName($name);

        if (null === $param) {
            return null;
        }

        return $param->getValue();
    }
    
    /**
     * Remove old file and upload a new one.
     * 
     * @param Request $request
     * @param Parameter $parameter
     * 
     * @return mixed
     */
    public  function uploadFile(Request $request, Parameter $parameter)
    {
        $file                = $request->files->get($parameter->getHtmlId());
        $args                = explode(';', $parameter->getArgs());
        $authorizedFileTypes = explode(',', $args[0]);
        $maxSize             = $args[1];
        if ($file === null) {
            return '';
        }

        if ($file->getClientSize() > UploadedFile::getMaxFilesize()) {
            return ['danger', 'parameter_file_size_error_max'];
        }

        if (($file->getClientSize()) > $maxSize * 1024) {
            return ['danger', 'parameter_file_size_error', $file->getClientSize()];
        }

        $fileType        = $file->getMimeType();
        $currentFileName = $parameter->getValue();

        if (!in_array($fileType, $authorizedFileTypes)) {
            return ['danger', 'parameter_file_type_error', $fileType];
        }

        $ext          = $file->guessExtension();
        $originalName = explode('.', $file->getClientOriginalName())[0];
        $fileName     = $originalName . '-' . substr(md5(uniqid()), 0, 4) . '.' . $ext;

        if ($currentFileName !== null && $currentFileName !== '') {
            unlink("files/$currentFileName");
        }

        $file->move('files', $fileName);

        return $fileName;
    }

}
