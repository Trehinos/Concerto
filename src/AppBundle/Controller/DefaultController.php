<?php

namespace AppBundle\Controller;

use Trehinos\ConcertoBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends BaseController
{

    /**
     * @Route("/", name="index")
     */
    public function indexAction()
    {
        return $this->render('App/Default/index.html.twig');
    }

}
